package com.emailApi.email;

import java.util.Hashtable;

import org.apache.velocity.VelocityContext;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.emailApi.model.EmailModel;
import com.emailApi.common.EmailManager;

@SpringBootApplication
@RequestMapping("/api")
public class Email {
	@RequestMapping(value = "/v1/email", method = RequestMethod.POST)
	public ResponseEntity<?> email(@RequestBody EmailModel jsondata) {
		Hashtable<Object, Object> map = new Hashtable<Object, Object>();
		try {
			VelocityContext vc = new VelocityContext();
			vc.put("license", jsondata.getLicense());
			vc.put("subject", jsondata.getSubject());
			vc.put("invoice", jsondata.getInvoice());
			vc.put("date", jsondata.getDate());
			vc.put("amount", jsondata.getAmount());
			vc.put("product", jsondata.getProduct());
			String email_template = EmailManager.email_message_template(vc, "email.vm");
			EmailManager.send_mail(jsondata.getEmail(), jsondata.getSubject(), email_template);

			map.put("responseCode", "00");
			map.put("responseMessage", "Email sent successfully");
			return ResponseEntity.ok(map);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("responseCode", "01");
			map.put("responseMessage", "Email not sent");
			map.put("error", e.getMessage());
			return ResponseEntity.ok(map);
		}
	}

	@RequestMapping(value = "/v1/user_added", method = RequestMethod.POST)
	public ResponseEntity<?> user_added(@RequestBody EmailModel jsondata) {
		Hashtable<Object, Object> map = new Hashtable<Object, Object>();
		Thread t = new Thread() {
			public void run() {
				try {
					VelocityContext vc = new VelocityContext();
					vc.put("email", jsondata.getEmail());
					vc.put("password", jsondata.getPassword());
					vc.put("subject", jsondata.getSubject());
					vc.put("message", jsondata.getMessage());
					String email_template = EmailManager.email_message_template(vc, "user_added.vm");
					EmailManager.send_mail(jsondata.getEmail(), jsondata.getSubject(), email_template);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		};
		t.start();
		map.put("responseCode", "00");
		map.put("responseMessage", "Email sent successfully");
		return ResponseEntity.ok(map);
	}
	@RequestMapping(value = "/v1/admin_invoice", method = RequestMethod.POST)
	public ResponseEntity<?> admin_invoice(@RequestBody EmailModel jsondata) {
		Hashtable<Object, Object> map = new Hashtable<Object, Object>();
		try {
			VelocityContext vc = new VelocityContext();
			vc.put("buyer_email", jsondata.getBuyer_email());
			vc.put("payment_method", jsondata.getPayment_method());
			vc.put("license", jsondata.getLicense());
			vc.put("subject", jsondata.getSubject());
			vc.put("invoice", jsondata.getInvoice());
			vc.put("date", jsondata.getDate());
			vc.put("amount", jsondata.getAmount());
			vc.put("product", jsondata.getProduct());
			String email_template = EmailManager.email_message_template(vc, "admin_invoice.vm");
			EmailManager.send_mail(jsondata.getEmail(), jsondata.getSubject(), email_template);

			map.put("responseCode", "00");
			map.put("responseMessage", "Email sent successfully");
			return ResponseEntity.ok(map);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("responseCode", "01");
			map.put("responseMessage", "Email not sent");
			map.put("error", e.getMessage());
			return ResponseEntity.ok(map);
		}
	}

}

